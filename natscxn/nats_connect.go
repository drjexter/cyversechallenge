package natscxn

import (
	"cyverse_challenge/error_handling"
	"github.com/nats-io/nats.go"
)

func StartNatsCxn(options []nats.Option) *nats.Conn{

	// Connect to NATS
	nc, err := nats.Connect(nats.DefaultURL,options...)
	error_handling.CheckErr(err)
	return nc
}
