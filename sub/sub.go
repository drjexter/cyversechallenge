package main

import (
	"cyverse_challenge/error_handling"
	"encoding/json"
	"log"

	//"context"
	"cyverse_challenge/mongo"
	"cyverse_challenge/natscxn"
	"cyverse_challenge/twitter"
	"github.com/nats-io/nats.go"
	"go.mongodb.org/mongo-driver/bson"
	"runtime"
)

func main() {
	// Connect to NATS
	nc := natscxn.StartNatsCxn(nil)
	subj := "twitter.feed"
	// Connect to MDB
	//TODO: Add better configuration for MongoDB, remove database and collection names to make configurable
	mdbCxn, ctx := mongo.GetMongoCxn()
	mdb := mongo.GetMongoDB(mdbCxn,"local")
	println(mdb.ListCollectionNames(ctx, bson.D{}))
	nc.Subscribe(subj, func(msg *nats.Msg) {
		tw := twitter.Tweet{}
		err := json.Unmarshal(msg.Data, &tw)
		error_handling.CheckErr(err)
		mongo.SendDocToMDB(mdb,"twitter_stream",ctx,tw)
	})
	nc.Flush()
	err := nc.LastError()
	error_handling.CheckErr(err)
	log.Printf("Listening on [%s]", subj)
	runtime.Goexit()
}
