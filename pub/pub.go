package main

import (
	"bufio"
	"cyverse_challenge/error_handling"
	"cyverse_challenge/natscxn"
	"cyverse_challenge/twitter"
	"log"
)

func main() {
	//NATS Setup
	nc := natscxn.StartNatsCxn(nil)
	defer nc.Close()
	subj := "twitter.feed"
	client := twitter.TwitterAPICxn()
	res := twitter.GetSampledStream(client)
	defer res.Body.Close()
	scanner := bufio.NewScanner(res.Body)
	//For each API hit publish to NATS
	log.Printf("Publishing on [%s]", subj)
	for scanner.Scan() {
		nc.Publish(subj, scanner.Bytes())
		nc.Flush()
		err := nc.LastError()
		error_handling.CheckErr(err)
	}
}
