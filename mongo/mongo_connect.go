package mongo

import (
	"context"
	"cyverse_challenge/error_handling"
	"cyverse_challenge/twitter"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

func GetMongoCxn() (*mongo.Client, context.Context) {
	client, err := mongo.NewClient(options.Client().ApplyURI(
		"mongodb://localhost:27017"))
	error_handling.CheckErr(err)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	error_handling.CheckErr(err)
	return client, ctx
}

func GetMongoDB(client *mongo.Client, dbName string) *mongo.Database {
	return client.Database(dbName)
}
func SendDocToMDB(database *mongo.Database, collection string, ctx context.Context, msg twitter.Tweet){
	database.Collection(collection).InsertOne(ctx,msg)
}