package twitter

import (
	"cyverse_challenge/error_handling"
	"net/http"
	"os"
)

type Tweet struct {
	Data struct {
		ID   string `json:"id"`
		Text string `json:"text"`
	} `json:"data"`
}

func TwitterAPICxn() *http.Client{
	//TWITTER API SETUP
	client := &http.Client{}
	return client
}

func addRules(client *http.Client) *http.Response{
	url := "https://api.twitter.com/2/tweets/search/stream/rules"
	method := "POST"
	bearerToken := os.Getenv("BEARER_TOKEN")
	//TODO: Add support to read rules json and add to body
	req, err := http.NewRequest(method, url, nil)
	error_handling.CheckErr(err)
	//Add Auth to header
	req.Header.Add("Authorization", "Bearer "+bearerToken)
	res, err := client.Do(req)
	error_handling.CheckErr(err)
	return res
}
func removeRules(client *http.Client) *http.Response{
	url := "https://api.twitter.com/2/tweets/search/stream/rules"
	method := "POST"
	bearerToken := os.Getenv("BEARER_TOKEN")
	//TODO: Add support to read rules_removal json and add to body
	req, err := http.NewRequest(method, url, nil)
	error_handling.CheckErr(err)
	//Add Auth to header
	req.Header.Add("Authorization", "Bearer "+bearerToken)
	res, err := client.Do(req)
	error_handling.CheckErr(err)
	return res
}
func getRules(client *http.Client) *http.Response{
	url := "https://api.twitter.com/2/tweets/search/stream/rules"
	method := "GET"
	bearerToken := os.Getenv("BEARER_TOKEN")
	req, err := http.NewRequest(method, url, nil)
	error_handling.CheckErr(err)
	//Add Auth to header
	req.Header.Add("Authorization", "Bearer "+bearerToken)
	res, err := client.Do(req)
	error_handling.CheckErr(err)
	return res
}
func GetFilteredStream(client *http.Client) *http.Response{
	url := "https://api.twitter.com/2/tweets/search/stream"
	method := "GET"
	bearerToken := os.Getenv("BEARER_TOKEN")
	req, err := http.NewRequest(method, url, nil)
	error_handling.CheckErr(err)
	//Add Auth to header
	req.Header.Add("Authorization", "Bearer "+bearerToken)
	res, err := client.Do(req)
	error_handling.CheckErr(err)
	return res
}
func GetSampledStream(client *http.Client) *http.Response{
	url := "https://api.twitter.com/2/tweets/sample/stream"
	method := "GET"
	bearerToken := os.Getenv("BEARER_TOKEN")
	req, err := http.NewRequest(method, url, nil)
	error_handling.CheckErr(err)
	//Add Auth to header
	req.Header.Add("Authorization", "Bearer "+bearerToken)
	res, err := client.Do(req)
	error_handling.CheckErr(err)
	return res
}