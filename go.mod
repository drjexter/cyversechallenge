module cyverse_challenge

go 1.15

require (
	github.com/nats-io/nats.go v1.10.0
	go.mongodb.org/mongo-driver v1.4.2
)
